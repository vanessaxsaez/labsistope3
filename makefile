DIR_OUTPUT = ./build
DIR_SRC = ./src
NOMBRE_EJECUTABLE = programa

# Esta es la target que se ejecuta por defecto si se escribe "make" en la consola
all: release

# Compilación principal del ejecutable
release:
	@echo 'Compilando target: $@'
	mkdir -p $(DIR_OUTPUT)
	gcc $(DIR_SRC)/programa1.c -o $(DIR_OUTPUT)/$(NOMBRE_EJECUTABLE)1 
	gcc $(DIR_SRC)/programa2.c -o $(DIR_OUTPUT)/$(NOMBRE_EJECUTABLE)2


	@echo ' '
	@echo $@ compilado en el directorio $(DIR_OUTPUT)
	@echo "Ingrese al directorio '"$(DIR_OUTPUT)"' haciendo: cd "$(DIR_OUTPUT)
	@echo "Ejecute el programa haciendo: ./"$(NOMBRE_EJECUTABLE)" "
	@echo ' '


# Other Targets
clean:
	#Borro el contenido del directorio de compilación
	rm -rf $(DIR_OUTPUT)/*
	-@echo ' ' 
