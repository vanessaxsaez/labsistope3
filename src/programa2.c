#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define TAMANO 536870912

int main(){

	int *array = (int *)malloc(sizeof(int)*TAMANO);
	srand(time(NULL));
	int i, randint;

	for (i=0; i<TAMANO; i++){
		randint = rand()%TAMANO;
		array[randint] = i;
	}
	return 0;
}